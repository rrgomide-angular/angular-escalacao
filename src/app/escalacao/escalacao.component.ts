import { Jogador } from './../interfaces/Jogador';
import { Component, OnInit } from '@angular/core';
import { EscalacaoService } from '../services/escalacao.service';

@Component({
  selector: 'app-escalacao',
  templateUrl: './escalacao.component.html',
  styleUrls: ['./escalacao.component.css']
})
export class EscalacaoComponent {
  constructor(public service: EscalacaoService) {}

  removerDaEscalacao(jogador: Jogador) {
    this.service.removerJogadorDaEscalacao(jogador);
  }
}
