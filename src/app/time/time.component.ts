import { Component, OnInit } from '@angular/core';
import { Jogador } from './../interfaces/Jogador';
import { EscalacaoService } from '../services/escalacao.service';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent {
  constructor(public service: EscalacaoService) {}

  inserirJogador(jogador: Jogador) {
    this.service.incluirJogadorNaEscalacao(jogador);
  }
}
