import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TimeComponent } from './time/time.component';
import { EscalacaoComponent } from './escalacao/escalacao.component';
import { EscalacaoService } from './services/escalacao.service';

@NgModule({
  declarations: [AppComponent, TimeComponent, EscalacaoComponent],
  imports: [BrowserModule],
  providers: [EscalacaoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
