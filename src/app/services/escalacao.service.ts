import { Jogador } from './../interfaces/Jogador';
import { Injectable } from '@angular/core';
import { GOLEIRO, ZAGUEIRO, MEIO_CAMPO, ATACANTE } from './../consts/Posicoes';

@Injectable({
  providedIn: 'root'
})
export class EscalacaoService {
  private _jogadores: Jogador[] = [];
  private _escalacao: Jogador[] = [];
  private _erro = '';

  private config = {};

  constructor() {
    this.config[GOLEIRO] = { vagas: 1, preenchidas: 0 };
    this.config[ZAGUEIRO] = { vagas: 3, preenchidas: 0 };
    this.config[MEIO_CAMPO] = { vagas: 5, preenchidas: 0 };
    this.config[ATACANTE] = { vagas: 2, preenchidas: 0 };

    this._jogadores.push(
      {
        posicao: 'Goleiro',
        nome: 'Rogério Ceni'
      },
      {
        posicao: 'Goleiro',
        nome: 'Zetti'
      },
      {
        posicao: 'Goleiro',
        nome: 'Sidão'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Ronaldão'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Rodrigo Caio'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Arboleda'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Bruno Alves'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Anderson Martins'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Ricardo Rocha'
      },
      {
        posicao: 'Zagueiro',
        nome: 'Maicon'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Hernanes'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Josué'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Mineiro'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Jucilei'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Nene'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Kaká'
      },
      {
        posicao: 'Meio-campo',
        nome: 'Raí'
      },
      {
        posicao: 'Atacante',
        nome: 'Diego Souza'
      },
      {
        posicao: 'Atacante',
        nome: 'Diego Souza'
      },
      {
        posicao: 'Atacante',
        nome: 'Lucas Pratto'
      },
      {
        posicao: 'Atacante',
        nome: 'Muller'
      },
      {
        posicao: 'Atacante',
        nome: 'Denílson'
      },
      {
        posicao: 'Atacante',
        nome: 'Lucas Moura'
      },
      {
        posicao: 'Atacante',
        nome: 'Amoroso'
      }
    );
  }

  removerJogadorDoTime(jogador) {
    this._jogadores = this._jogadores.filter(item => {
      return item.nome !== jogador.nome;
    });
  }

  incluirJogadorNaEscalacao(jogador: Jogador): void {
    /**
     * Verificando se jogador
     * já foi incluído
     */
    const nomeJogador = jogador.nome;

    if (this._escalacao.some(item => item.nome === nomeJogador)) {
      this._erro = 'Jogador ' + nomeJogador + ' já foi incluído!';
      return;
    }

    const posicaoJogador = jogador.posicao;

    /**
     * Verificando se há vagas
     */
    if (
      this.config[posicaoJogador].preenchidas <
      this.config[posicaoJogador].vagas
    ) {
      this._escalacao.push(jogador);
      this.config[posicaoJogador].preenchidas++;
      this._erro = '';

      this.removerJogadorDoTime(jogador);
    } else {
      this._erro = 'Todas as vagas de ' + posicaoJogador + ' preenchidas!';
    }
  }

  incluirJogadorNoTime(jogador) {
    this._jogadores.push(jogador);
  }

  removerJogadorDaEscalacao(jogador) {
    console.log(jogador);
    console.log(this._escalacao);
    this._escalacao = this._escalacao.filter(item => {
      return item.nome !== jogador.nome;
    });

    this.config[jogador.posicao].preenchidas--;

    this.incluirJogadorNoTime(jogador);
  }

  get todosOsJogadores() {
    return [].concat(this._jogadores);
  }

  get escalacao() {
    return [].concat(this._escalacao);
  }

  get timeCompleto() {
    const timeCompleto = Object.keys(this.config).every(key => {
      const vagas = this.config[key].vagas;
      const preenchidas = this.config[key].preenchidas;
      return vagas === preenchidas;
    });

    return timeCompleto;
  }

  get erro() {
    return this._erro;
  }
}
